# vue-life1

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Links

- Vue.js
  - https://jp.vuejs.org/v2/guide/
  - https://cli.vuejs.org/guide/installation.html
- https://codepen.io/B_Sanchez/pen/yzzXZo
